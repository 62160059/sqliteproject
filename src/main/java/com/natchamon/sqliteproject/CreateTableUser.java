/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natchamon.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ADMIN
 */
public class CreateTableUser {
    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
                   stmt = conn.createStatement();

                   String sql = "CREATE TABLE USER" +
                           "(ID INT PRIMARY KEY NOT NULL," +
                           "NAME TEXT NOT NULL," +
                           "AGE INT NOT NULL," +
                           "SEX CHAR(10)," +
                           "ADDRESS CHAR(50)," +
                           "SALARY REAL)";
                   stmt.executeUpdate(sql);
                   stmt.close();
                   conn.close();
                   
                   
        } catch (ClassNotFoundException ex) {
            System.out.println("No library org.sqlite.JDBC");
             System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connection database!!!");
             System.exit(0);
        }

        
    }
}
